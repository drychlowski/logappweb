﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LogAppWebCore.Data;
using LogAppWebCore.Models;
using LogAppWebCore.Models.UserViewModels;
using LogAppWebCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

namespace LogAppWebCore.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public UsersController(UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [Authorize(Policy = "UsersView" )]
        public async Task<IActionResult> Index(string statusMessage)
        {
            var userList = await _userManager.Users.Where(u => !u.UserName.Equals("admin", StringComparison.CurrentCultureIgnoreCase)).ToListAsync();

            var users = userList.Select(o => new UserEditViewModel
                {
                    Id = o.Id,
                    Name = o.Name,
                    Surname = o.Surname,
                    UserName = o.Surname,
                })
                .ToList();

            var model = new UserListViewModel
            {
                StatusMessage = statusMessage,
                Users = users
            };

            return View(model);
        }

        [Authorize(Policy = "UsersView")]
        public async Task<IActionResult> Details(int id)
        {
            var user = await _userManager.Users.Include(o => o.Roles).ThenInclude(o => o.Role).FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
                return RedirectToAction("Index");

            var roleNames = user.Roles.Select(o => o.Role.Name);
            var roles = String.Join(", ", roleNames);

            var model = new UserDetailsViewModel
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                UserName = user.UserName,
                Email = user.Email,
                Active = user.Active,
                Roles = roles
            };

            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "UsersCreate")]
        public async Task<IActionResult> Create()
        {
            var applicationRoles = await _roleManager.Roles.ToListAsync();

            var roles = applicationRoles.Select(applicationRole => new RoleViewModel
                {
                    Id = applicationRole.Id,
                    Name = applicationRole.Name
                })
                .ToList();

            var model = new UserEditViewModel
            {
                Roles = roles
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "UsersCreate")]
        public async Task<IActionResult> Create(UserEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Create");
            }

            var newUser = new User
            {
                Name = model.Name,
                Surname = model.Surname,
                UserName = model.UserName,
                Email = model.Email,
                Active = true,
                ChangePassword = true
            };
            var password = PasswordGenerator.GenerateRandomPassword();

            var result = await _userManager.CreateAsync(newUser, password);

            if (result.Succeeded)
            {
                foreach (var role in model.Roles.Where(role => role.IsSelected))
                    await _userManager.AddToRoleAsync(newUser, role.Name);

                model.StatusMessage = $"User created successfully with password: {password}";
                return View(model);
            }

            AddErrors(result);
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "UsersEdit")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
                return RedirectToAction("Index");

            var roleViewModelList = new List<RoleViewModel>();
            var applicationRoles = await _roleManager.Roles.ToListAsync();

            foreach (var applicationRole in applicationRoles)
            {
                roleViewModelList.Add(new RoleViewModel
                {
                    Id = applicationRole.Id,
                    Name = applicationRole.Name,
                    IsSelected = await _userManager.IsInRoleAsync(user, applicationRole.Name)
                });
            }

            var model = new UserEditViewModel
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                UserName = user.UserName,
                Email = user.Email,
                Active = user.Active,
                Roles = roleViewModelList
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "UsersEdit")]
        public async Task<IActionResult> Edit(UserEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }

            var user = await _userManager.Users.FirstOrDefaultAsync(o => o.Id == model.Id);

            user.Name = model.Name;
            user.Surname = model.Surname;
            user.UserName = model.UserName;
            user.Email = model.Email;
            user.Active = model.Active;

            foreach (var role in model.Roles)
            {
                if (role.IsSelected)
                    await _userManager.AddToRoleAsync(user, role.Name);
                else
                    await _userManager.RemoveFromRoleAsync(user, role.Name);
            }

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                model.StatusMessage = "User updated successfully.";

                if (!model.Active)
                    await _userManager.UpdateSecurityStampAsync(user);

                return View(model);
            }

            AddErrors(result);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "UsersDelete")]
        public async Task<IActionResult> Delete(int id)
        {
            var userToDelete = await _userManager.Users.FirstOrDefaultAsync(u => u.Id == id);
            await _userManager.DeleteAsync(userToDelete);

            return RedirectToAction("Index", new {StatusMessage = "User deleted succesfully."});
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}