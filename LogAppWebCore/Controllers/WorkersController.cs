﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Models;
using LogAppWebCore.Models.WorkersViewModels;
using LogAppWebCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LogAppWebCore.Controllers
{
    [Authorize]
    public class WorkersController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly WorkLogService _workLogService;
        private readonly WorkerService _managerService;

        public WorkersController(UserManager<User> userManager, WorkLogService workLogService, WorkerService managerService)
        {
            _userManager = userManager;
            _workLogService = workLogService;
            _managerService = managerService;
        }

        [Authorize(Policy = "WorkersView")]
        public IActionResult Index()
        {
            var workers = _managerService.GetWorkers();

            var workerListViewModel = workers.Select(o => new WorkerDetailsViewModel
            {
                Id = o.Id,
                Name = o.Name,
                Surname = o.Surname,
            })
            .ToList();

            var model = new WorkerListViewModel
            {
                Workers = workerListViewModel
            };

            return View(model);
        }

        [Authorize(Policy = "WorkersView")]
        public async Task<IActionResult> Details(int workerId)
        {
            var worker = await _userManager.Users.SingleOrDefaultAsync(u => u.Id == workerId);

            if (worker == null)
                return RedirectToAction("Index");

            var model = new WorkerDetailsViewModel
            {
                Name = worker.Name,
                Surname = worker.Surname,
                Email = worker.Email,
            };

            return View(model);
        }

        /*
        public IActionResult WorkerWorkLogs(int workerId)
        {
            var user = _userManager.Users.SingleOrDefault(o => o.Id == workerId);
            var workLogs = _workLogService.GetUserWorkLogsWithUser(user);

            var workLogListViewModel = workLogs.Select(o => new WorkLogViewModel
                {
                    UserName = o.User.Name,
                    UserSurname = o.User.Surname,
                    WorkDate = o.WorkStartTime.ToString("d"),
                    WorkStartTime = o.WorkStartTime.ToString("t"),
                    WorkEndTime = o.WorkEndTime.ToString("t"),
                    WorkDuration = o.WorkDuration.ToString(@"hh\:mm")
                })
                .ToList();

            var model = new WorkLogListViewModel
            {
                WorkLogs = workLogListViewModel
            };

            return View(model);
        }

        public IActionResult WorkerWorkLogDetails(int workLogId)
        {
            var workLog = _workLogService.GetWorkLogWithUser(workLogId);

            if (workLog == null)
                return RedirectToAction("Index");

            var model = new WorkLogViewModel
            {
                UserName = workLog.User.Name,
                UserSurname = workLog.User.Surname,
                WorkDate = workLog.WorkStartTime.ToString("d"),
                WorkStartTime = workLog.WorkStartTime.ToString("t"),
                WorkEndTime = workLog.WorkEndTime.ToString("t"),
                WorkDuration = workLog.WorkDuration.ToString(@"hh\:mm")
            };

            return View(model);
        }
        */
    }
}