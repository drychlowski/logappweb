﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Models;
using LogAppWebCore.Models.WorkerViewModels;
using LogAppWebCore.Models.WorkLogViewModels;
using LogAppWebCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LogAppWebCore.Controllers
{
    [Authorize(Policy = "IsWorker")]
    public class WorkerController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly WorkLogService _workLogService;

        public WorkerController(UserManager<User> userManager, WorkLogService workLogService)
        {
            _userManager = userManager;
            _workLogService = workLogService;
        }

        public IActionResult Index()
        {
            return View("Index");
        }

        public async Task<IActionResult> WorkLogs()
        {
            var user = await _userManager.Users.Include(o => o.WorkLogs)
                .FirstOrDefaultAsync(o => o.UserName == User.Identity.Name);

            var index = 1;
            var workLogsVm = user.WorkLogs.Select(o => new WorkerWorkLogViewModel
            {
                Number = index++,
                Id = o.Id,
                WorkDate = o.WorkStartTime.ToString("d"),
                WorkStartTime = o.WorkStartTime.ToString("t"),
                WorkEndTime = o.WorkEndTime.ToString("t"),
                WorkDuration = o.WorkDuration.ToString(@"hh\:mm")
            })
            .ToList();

            var model = new WorkerWorkLogListViewModel
            {
                WorkLogs = workLogsVm
            };

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> WorkLogDetails(int id)
        {
            var user = await _userManager.Users.Include(o => o.WorkLogs)
                .FirstOrDefaultAsync(o => o.UserName == User.Identity.Name);

            if (user == null)
                return RedirectToAction("WorkLogs");

            var workLog = user.WorkLogs.FirstOrDefault(o => o.Id == id);

            if (workLog == null)
                return RedirectToAction("WorkLogs");

            var model = new WorkerWorkLogViewModel
            {
                WorkDate = workLog.WorkStartTime.ToString("d"),
                WorkStartTime = workLog.WorkStartTime.ToString("t"),
                WorkEndTime = workLog.WorkEndTime.ToString("t"),
                WorkDuration = workLog.WorkDuration.ToString(@"hh\:mm")
            };

            return View(model);
        }
    }
}
