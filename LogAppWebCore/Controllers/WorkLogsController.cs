﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Models;
using LogAppWebCore.Models.WorkLogViewModels;
using LogAppWebCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LogAppWebCore.Controllers
{
    public class WorkLogsController : Controller
    {
        private readonly WorkLogService _workLogService;
        private readonly UserManager<User> _userManager;

        public WorkLogsController(WorkLogService workLogService, UserManager<User> userManager)
        {
            _workLogService = workLogService;
            _userManager = userManager;
        }

        [Authorize(Policy = "WorkLogsView")]
        public IActionResult Index(int? workerId)
        {
            IEnumerable<WorkLog> workLogs;
            if (workerId == null)
            {
                workLogs = _workLogService.GetAllWorkLogsWithUser();
            }
            else
            {
                var user = _userManager.Users.SingleOrDefault(o => o.Id == workerId);
                if (user == null)
                    return RedirectToAction("Index");

                workLogs = _workLogService.GetUserWorkLogsWithUser(user);
            }

            var workLogListViewModel = workLogs.Select(o => new WorkLogViewModel
            {
                Id = o.Id,
                UserId = o.UserId,
                UserUsername = o.User.UserName,
                UserName = o.User.Name,
                UserSurname = o.User.Surname,
                WorkDate = o.WorkStartTime.ToString("d"),
                WorkStartTime = o.WorkStartTime.ToString("t"),
                WorkEndTime = o.WorkEndTime.ToString("t"),
                WorkDuration = o.WorkDuration.ToString(@"hh\:mm")
            })
            .ToList();

            var model = new WorkLogListViewModel
            {
                WorkLogs = workLogListViewModel
            };

            return View(model);
        }

        /*
        public IActionResult WorkerWorkLogs(int workerId)
        {
            var worker = _userManager.Users.SingleOrDefault(o => o.Id == workerId);

            if (worker == null)
                return RedirectToAction("Index");

            var workLogs = _workLogService.GetUserWorkLogs(worker);

            var workLogListViewModel = workLogs.Select(o => new WorkLogViewModel
                {
                    Id = o.Id,
                    UserId = worker.Id,
                    UserUsername = worker.UserName,
                    UserName = worker.Name,
                    UserSurname = worker.Surname,
                    WorkDate = o.WorkStartTime.ToString("d"),
                    WorkStartTime = o.WorkStartTime.ToString("t"),
                    WorkEndTime = o.WorkEndTime.ToString("t"),
                    WorkDuration = o.WorkDuration.ToString(@"hh\:mm")
                })
                .ToList();

            var model = new WorkLogListViewModel
            {
                WorkLogs = workLogListViewModel
            };

            return View(model);
        }
        */

        public IActionResult Details(int workLogId)
        {
            var workLog = _workLogService.GetWorkLogWithUser(workLogId);

            if (workLog == null)
                return RedirectToAction("Index");

            var model = new WorkLogViewModel
            {
                UserName = workLog.User.Name,
                UserSurname = workLog.User.Surname,
                WorkDate = workLog.WorkStartTime.ToString("d"),
                WorkStartTime = workLog.WorkStartTime.ToString("t"),
                WorkEndTime = workLog.WorkEndTime.ToString("t"),
                WorkDuration = workLog.WorkDuration.ToString(@"hh\:mm")
            };

            return View(model);
        }
    }
}