﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LogAppWebCore.Models;
using LogAppWebCore.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LogAppWebCore.Data
{
    public static class DbInitializer
    {
        private static async Task SeedRoles(RoleManager<Role> roleManager, AppDbContext context, IPermissionService permissionService)
        {
            //add roles to db
            string[] roles = { "Admin", "Manager", "Worker" };
            foreach (var role in roles)
            {
                if (!await roleManager.RoleExistsAsync(role))
                    await roleManager.CreateAsync(new Role(role));
            }

            //add all permissions to db
            foreach (var permissionItem in Permissions.GetPermissionItems())
            {
                foreach (var permissionAction in Permissions.GetPermissionActions())
                {
                    var permissionExist = permissionService.PermissionExist(permissionItem, permissionAction);
                    if (permissionExist)
                        continue;

                    var permission = new Permission
                    {
                        PermissionItem = permissionItem.ToString(),
                        PermissionAction = permissionAction.ToString()
                    };
                    
                    context.Permissions.Add(permission);
                }
            }
            context.SaveChanges();

            //add permissions to admin
            var adminRole = await roleManager.FindByNameAsync("Admin");

            var usersViewPermission = permissionService.GetPermission(Permissions.Item.Users, Permissions.Action.View);
            permissionService.AddPermissionToRole(adminRole, usersViewPermission);

            var usersCreatePermission = permissionService.GetPermission(Permissions.Item.Users, Permissions.Action.Create);
            permissionService.AddPermissionToRole(adminRole, usersCreatePermission);

            var usersEditPermission = permissionService.GetPermission(Permissions.Item.Users, Permissions.Action.Edit);
            permissionService.AddPermissionToRole(adminRole, usersEditPermission);

            var usersDeletePermission = permissionService.GetPermission(Permissions.Item.Users, Permissions.Action.Delete);
            permissionService.AddPermissionToRole(adminRole, usersDeletePermission);

            //add permissions to manager
            var managerRole = await roleManager.FindByNameAsync("Manager");

            var workersViewPermission = permissionService.GetPermission(Permissions.Item.Workers, Permissions.Action.View);
            permissionService.AddPermissionToRole(managerRole, workersViewPermission);

            var workLogsCreatePermission = permissionService.GetPermission(Permissions.Item.WorkLogs, Permissions.Action.View);
            permissionService.AddPermissionToRole(managerRole, workLogsCreatePermission);
        }

        private static async Task SeedUsers(UserManager<User> userManager)
        {
            for (var i = 0; i < 2; i++)
            {
                var manager = new User
                {
                    UserName = $"manager{i}",
                    Email = $"manager{i}@email.com",
                    Name = $"Manager{i}",
                    Surname = $"Manager{i}",
                    Active = true,
                    ChangePassword = true
                };
                var password = "Password11";

                var createUser = await userManager.CreateAsync(manager, password);
                if (!createUser.Succeeded) continue;
                await userManager.AddToRoleAsync(manager, "Manager");
            }

            for (var i = 0; i < 20; i++)
            {
                var user = new User
                {
                    UserName = $"User{i}",
                    Email = $"user{i}@email.com",
                    Name = $"User{i}",
                    Surname = $"User{i}",
                    Active = true,
                    ChangePassword = true
                };
                var password = "Password11";

                var createUser = await userManager.CreateAsync(user, password);
                if (!createUser.Succeeded) continue;
                await userManager.AddToRoleAsync(user, "Worker");
            }
        }

        private static async Task AddAdmin(UserManager<User> userManager)
        {
            var admin = new User
            {
                UserName = "Admin",
                Email = "admin@email.com",
                Name = "Admin",
                Surname = "Admin",
                Active = true
            };
            const string adminPassword = "Password11";

            var createAdmin = await userManager.CreateAsync(admin, adminPassword);
            if (createAdmin.Succeeded)
                await userManager.AddToRoleAsync(admin, "Admin");
        }
        

        public static async Task<IWebHost> SeedData(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var userManager = scope.ServiceProvider.GetService<UserManager<User>>();
                var roleManager = scope.ServiceProvider.GetService<RoleManager<Role>>();
                var permissionService = scope.ServiceProvider.GetService<IPermissionService>();
                var context = scope.ServiceProvider.GetService<AppDbContext>();

                await SeedRoles(roleManager, context, permissionService);
                await SeedUsers(userManager);
                await AddAdmin(userManager);

            }
            return host;
        }
    }
}
