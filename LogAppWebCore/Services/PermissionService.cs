﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Data;
using LogAppWebCore.Models;
using Microsoft.EntityFrameworkCore;

namespace LogAppWebCore.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly AppDbContext _context;

        public PermissionService(AppDbContext context)
        {
            _context = context;
        }

        public bool CheckPermissionForUser(string userName, string permissionName)
        {
            var user = _context.Users
                .Include(o => o.Roles)
                .ThenInclude(o => o.Role)
                .ThenInclude(o => o.RolePermissions)
                .ThenInclude(o => o.Permission)
                .SingleOrDefault(o => o.UserName == userName);

            if (user == null)
                return false;

            var userRoles = user.Roles.Select(o => o.Role);
            var userPermissions = userRoles.SelectMany(o => o.RolePermissions).Select(o => o.Permission).Select(o => o.Name);

            return userPermissions.Contains(permissionName);
        }

        public bool PermissionExist(Permissions.Item item, Permissions.Action action)
        {
            var permission = _context.Permissions.SingleOrDefault(o => o.PermissionItem == item.ToString() && o.PermissionAction == action.ToString());

            return permission != null;
        }

        public void AddPermissionToRole(Role role, Permission permission)
        {
            if (role == null || permission == null)
                throw new ArgumentNullException();

            var roleToEdit = _context.Roles.Include(o => o.RolePermissions).ThenInclude(o => o.Permission).SingleOrDefault(o => o == role);
            var permissionToEdit = _context.Permissions.SingleOrDefault(o => o == permission);

            if (roleToEdit == null || permissionToEdit == null)
                throw new NullReferenceException();

            if (roleToEdit.RolePermissions.Select(o => o.Permission).Contains(permissionToEdit))
                return;

            var rolePermission = new RolePermission
            {
                Permission = permissionToEdit,
                Role = roleToEdit
            };

            _context.RolePermissions.Add(rolePermission);
            _context.SaveChanges();
        }

        public Permission GetPermissionByName(string permissionName)
        {
            if (permissionName == null) throw new ArgumentNullException(nameof(permissionName));

            return _context.Permissions.SingleOrDefault(o => o.Name == permissionName);
        }

        public Permission GetPermission(Permissions.Item item, Permissions.Action action)
        {
            return _context.Permissions.SingleOrDefault(o => o.PermissionItem == item.ToString() && o.PermissionAction == action.ToString());
        }
    }
}
