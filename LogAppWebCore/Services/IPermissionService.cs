﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Models;

namespace LogAppWebCore.Services
{
    public interface IPermissionService
    {
        bool CheckPermissionForUser(string userName, string permissionName);
        bool PermissionExist(Permissions.Item item, Permissions.Action action);
        void AddPermissionToRole(Role role, Permission permission);
        Permission GetPermissionByName(string permissionName);
        Permission GetPermission(Permissions.Item item, Permissions.Action action);


    }

}
