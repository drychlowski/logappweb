﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Data;
using LogAppWebCore.Models;
using Microsoft.EntityFrameworkCore;

namespace LogAppWebCore.Services
{
    public class WorkerService
    {
        private readonly AppDbContext _context;

        public WorkerService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetWorkers()
        {
            var workerRole = _context.Roles.Include(o => o.Users).ThenInclude(o => o.User)
                .SingleOrDefault(o => o.Name.Equals("Worker", StringComparison.CurrentCultureIgnoreCase));

            if (workerRole == null)
            {
                throw new Exception("Missing Worker role");
            }

            var workers = workerRole.Users.Select(o => o.User).ToList();
            return workers;
        }

        public int GetWorkersCount()
        {
            var workers = GetWorkers();
            return workers.Count();
        }
    }
}
