﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Data;
using LogAppWebCore.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using AppDbContext = LogAppWebCore.Data.AppDbContext;


namespace LogAppWebCore.Services
{
    public class WorkLogService
    {
        private readonly AppDbContext _context;

        public WorkLogService(AppDbContext context)
        {
            _context = context;
        }

        public void AddWorkLog(User user, DateTime workStartTime, DateTime workEndTIme)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var workLog = new WorkLog
            {
                UserId = user.Id,
                WorkStartTime = workStartTime,
                WorkEndTime = workEndTIme,
                WorkDuration = workEndTIme - workStartTime
            };

            _context.WorkLogs.Add(workLog);
            _context.SaveChanges();
        }

        public IEnumerable<WorkLog> GetAllWorkLogs()
        {
            return _context.WorkLogs.ToList();
        }

        public IEnumerable<WorkLog> GetAllWorkLogsWithUser()
        {
            return _context.WorkLogs.Include(o => o.User).ToList();
        }

        public IEnumerable<WorkLog> GetUserWorkLogs(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            return _context.WorkLogs.Where(o => o.UserId == user.Id).ToList();
        }

        public IEnumerable<WorkLog> GetUserWorkLogsWithUser(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            return _context.WorkLogs.Include(o => o.User).Where(o => o.UserId == user.Id).ToList();
        }

        public WorkLog GetWorkLog(int id)
        {
            return _context.WorkLogs.SingleOrDefault(o => o.Id == id);
        }

        public WorkLog GetWorkLogWithUser(int id)
        {
            return _context.WorkLogs.Include(o => o.User).SingleOrDefault(o => o.Id == id);
        }

        public DateTime GetTotalWorkingTime()
        {
            var totalTime = new DateTime();
            totalTime = GetAllWorkLogs().Aggregate(totalTime, (current, worklog) => current + worklog.WorkDuration);

            return totalTime;
        }
    }
}
