﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogAppWebCore.Models;
using Microsoft.AspNetCore.Authorization;

namespace LogAppWebCore.Extensions
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public string PermissionName { get; }

        public PermissionRequirement(Permissions.Item item, Permissions.Action action)
        {
            PermissionName = item.ToString() + action;
        }
    }
}
