﻿"use strict";

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('wwwroot/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('wwwroot/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('wwwroot/sass/*.scss', gulp.series('sass'));
});

gulp.task('fonts',
    function() {
        return gulp.src('node_modules/font-awesome/fonts/*')
            .pipe(gulp.dest('wwwroot/fonts'));
    });