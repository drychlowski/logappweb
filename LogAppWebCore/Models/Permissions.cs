﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models
{
    public static class Permissions
    {
        public enum Item
        {
            Users,
            Workers,
            WorkLogs
        }

        public enum Action
        {
            View,
            Create,
            Edit,
            Delete
        }

        public static IEnumerable<Item> GetPermissionItems()
        {
            foreach (var item in (Item[])Enum.GetValues(typeof(Item)))
            {
                yield return item;
            }
        }

        public static IEnumerable<Action> GetPermissionActions()
        {
            foreach (var action in (Action[])Enum.GetValues(typeof(Action)))
            {
                yield return action;
            }
        }
    }
}
