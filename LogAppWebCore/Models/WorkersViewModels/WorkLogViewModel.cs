﻿using System.ComponentModel.DataAnnotations;

namespace LogAppWebCore.Models.WorkersViewModels
{
    public class WorkLogViewModel
    {
        [Display(Name = "Worker name")]
        public string UserName { get; set; }

        [Display(Name = "Worker surname")]
        public string UserSurname { get; set; }

        [Display(Name = "Date of work")]
        public string WorkDate { get; set; }

        [Display(Name = "Time of start of work")]
        public string WorkStartTime { get; set; }

        [Display(Name = "Time of end of work")]
        public string WorkEndTime { get; set; }

        [Display(Name = "Duration of work")]
        public string WorkDuration { get; set; }
    }
}
