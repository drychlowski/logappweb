﻿using System.Collections.Generic;

namespace LogAppWebCore.Models.WorkersViewModels
{
    public class WorkerListViewModel
    {
        public List<WorkerDetailsViewModel> Workers { get; set; }
    }
}
