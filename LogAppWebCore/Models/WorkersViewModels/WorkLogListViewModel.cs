﻿using System.Collections.Generic;

namespace LogAppWebCore.Models.WorkersViewModels
{
    public class WorkLogListViewModel
    {
        public List<WorkLogViewModel> WorkLogs { get; set; }
    }
}
