﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models.WorkerViewModels
{
    public class WorkerWorkLogListViewModel
    {
        public List<WorkerWorkLogViewModel> WorkLogs { get; set; }
    }
}
