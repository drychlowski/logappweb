﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models.UserViewModels
{
    public class UserDetailsViewModel
    {
        [Required]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        [Display(Name = "Status")]
        public string ActiveText => Active ? "Active" : "Deactivated";

        [Display(Name = "Roles")]
        public string Roles { get; set; }
    }
}
