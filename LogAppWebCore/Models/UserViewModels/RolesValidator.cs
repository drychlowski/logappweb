﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models.UserViewModels
{
    public class RolesValidator : ValidationAttribute
    {
        public RolesValidator()
        {
            ErrorMessage = "At least one role is required.";
        }

        public override bool IsValid(object value)
        {
            if (!(value is List<RoleViewModel> roles))
                return false;

            return roles.Any(role => role.IsSelected);
        }
    }

}
