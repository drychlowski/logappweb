﻿using System.Collections.Generic;

namespace LogAppWebCore.Models.UserViewModels
{
    public class UserListViewModel
    {
        public List<UserEditViewModel> Users { get; set; }

        public string StatusMessage { get; set; }
    }
}
