﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LogAppWebCore.Models.UserViewModels
{
    public class UserEditViewModel
    {
        [Required]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Username")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Status")]
        public bool Active { get; set; }

        [Required]
        [Display(Name = "Change password")]
        public bool ChangePassword { get; set; }

        [Required]
        //[RolesValidator]
        public List<RoleViewModel> Roles { get; set; }


        public string StatusMessage { get; set; }
    }
}
