﻿using System.ComponentModel.DataAnnotations;

namespace LogAppWebCore.Models.UserViewModels
{
    public class RoleViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool IsSelected { get; set; }
    }
}
