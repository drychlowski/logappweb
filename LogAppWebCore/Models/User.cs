﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace LogAppWebCore.Models
{
    public class User : IdentityUser<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public bool ChangePassword { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<WorkLog> WorkLogs { get; set; }
    }
}
