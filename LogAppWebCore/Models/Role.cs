﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace LogAppWebCore.Models
{
    public class Role : IdentityRole<int>
    {
        public virtual ICollection<UserRole> Users { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }

        public Role()
        { }

        public Role(string roleName) : base(roleName)
        { }
    }
}
