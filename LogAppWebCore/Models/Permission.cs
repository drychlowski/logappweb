﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models
{
    public class Permission
    {
        public int Id { get; set; }

        public string Name => PermissionItem + PermissionAction;

        public string PermissionItem { get; set; }

        public string PermissionAction { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }
    }
}
