﻿using System.Collections.Generic;

namespace LogAppWebCore.Models.WorkLogViewModels
{
    public class WorkLogListViewModel
    {
        public List<WorkLogViewModel> WorkLogs { get; set; }
    }
}
