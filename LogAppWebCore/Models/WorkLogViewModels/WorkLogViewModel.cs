﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models.WorkLogViewModels
{
    public class WorkLogViewModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        [Display(Name = "Worker username")]
        public string UserUsername { get; set; }

        [Display(Name = "Worker name")]
        public string UserName { get; set; }

        [Display(Name = "Worker surname")]
        public string UserSurname { get; set; }

        [Display(Name = "Date of work")]
        public string WorkDate { get; set; }

        [Display(Name = "Time of start of work")]
        public string WorkStartTime { get; set; }

        [Display(Name = "Time of end of work")]
        public string WorkEndTime { get; set; }

        [Display(Name = "Duration of work")]
        public string WorkDuration { get; set; }
    }
}
