﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogAppWebCore.Models
{
    public class WorkLog
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public DateTime WorkStartTime { get; set; }

        public DateTime WorkEndTime { get; set; }

        public TimeSpan WorkDuration { get; set; }
    }
}
